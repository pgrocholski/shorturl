# Synopsis
A simple url-shortening webapp in Mojolicious

# Description
Just a simple url shortener in Mojolicious using DBIC for the model (SQLite), as well as simple read and well as using Cache::Memcached::Fast for quick shorturl lookup, done in a read/write-through fashion.

# Notes
-Be advised that the cache is not invalidated, thus will likely become stale if the model grows more complex (if url deletion/modification is added)
-Shorturl codes are sequential and predictable, thus are subject to (hil|nef)arious shorturl hacks as tinyurl was back in the day

# Requirements
- Mojolicious
- Math::Base36
- DBIx::Class
- Cache::Memcached::Fast
- Data::Validate::URI

# Use
morbo shorturl
or
hypnotoad shorturl

