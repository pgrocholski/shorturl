package ShortURL::Schema::Result::ShortURL;
use strict;
use warnings;
use Math::Base36 qw/encode_base36/;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('shorturl');
__PACKAGE__->add_columns(qw/id url/);
__PACKAGE__->set_primary_key('id');

#the shorturl is a calculated field 
#TODO Make lazy/cached
sub shorturl {
    my $self = shift;
    return encode_base36($self->id);
}

1;
