use Test::More;
use Test::Mojo;

use FindBin;
$ENV{MOJO_HOME} = "$FindBin::Bin/../";
require "$ENV{MOJO_HOME}/shorturl";

my $t = Test::Mojo->new;

my @valid =     qw{ http://slashdot.org
                    http://arstechnica.com
                    http://news.ycombinator.com
                    https://www.google.com
                    http://google.com
                    http://reddit.org/r/programming
                    http://www.reddit.com/search?q=shorturl
                    https://somereallylongurlorsomethingofthatsort.org/with/a/long/path?and=such
                    http://perl.org
                    http://mojolicio.us
                    http://metacpan.org
                    http://reddit.org};

my @invalid =   qw{ ftp://something.org
                    derp://meh.
                    http://derp.
                    http:///derp.org
                    http:://something.org
                    http//sometihng.org
                    http:/something.org};

my %shortcodes;

#check that we can load the main form
$t->get_ok('/');

#post urls and get the shortcodes back
for my $url (@valid) {
    $shortcodes{$url} = $t->post_form_ok('/' => {url => $url})->status_is(200)->tx->res->dom('a')->[0]->text;
}

#ensure that non-valid urls dont pass validation
for my $url (@invalid) {
    $t->post_form_ok('/' => {url => $url})->status_is(400);

}

while (my ($url, $code) = each(%shortcodes)) {
    #make sure that the same url gets the same shorturl back
    $t->post_form_ok('/' => {url => $url})
        ->status_is(200)
        ->text_is('a' => $code);

    #check whether the redirect for a code has the right url
    $t->get_ok($code)
        ->status_is(200)
        ->content_like(qr{url=\Q$url})
        ->content_like(qr{ref="\Q$url"});
}

done_testing;
