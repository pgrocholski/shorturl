CREATE TABLE IF NOT EXISTS shorturl (
    id INTEGER PRIMARY KEY autoincrement,
    url TEXT UNIQUE NOT NULL
);
CREATE INDEX IF NOT EXISTS url_index on shorturl (url);
